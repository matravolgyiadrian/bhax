//Végtelen ciklus, mely 100%-on terheli az összes magot!
//Így futtasd:
//  gcc -fopenmp allcores.c -o main
//  ./main
#include <omp.h>

int main() {
        #pragma omp parallel
        while(1){}
        return 0;
}

//Végtelen ciklus, mely 100%-on terhel 1 magot!
//Így futtasd:
//  gcc onecore.c -o main
//  ./main
int main() {
        while(1){}
        return 0;
}

//Végtelen ciklus, mely 0%-on terhel 1 magot!
//Így futtasd:
//  gcc zero.c -o main
//  ./main
#include <unistd.h>

int main() {
           while(1){
               sleep(1)
           }
        return 0;
}
