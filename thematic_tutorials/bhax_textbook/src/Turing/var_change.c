#include <stdio.h>
int main()
{
    int a=5;
    int b=10;

    int c=a;
    a=b;
    b=c;
    //szorzással, osztásal
    // a=a*b;
    // b=a/b;
    // a=a/b;

    //Összeadással, kivonással
    // a = a + b;
    // b = a - b;
    // a = a - b;

    // kizáró vagy-gyal
    // a = a ^ b;
    // b = a ^ b;
    // a = a ^ b;
    //
    printf("%d, %d\n",a,b );
}
